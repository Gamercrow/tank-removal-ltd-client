from dtos.PlayerDTO import PlayerDTO


class KillDTO:

    def __init__(self, killer: PlayerDTO, victim: PlayerDTO, weapon: str, vehicle: str, continent: str,
                 server_id: int, is_revenge: bool):
        self.killer = killer
        self.victim = victim
        self.weapon = weapon
        self.continent = continent
        self.vehicle = vehicle
        self.server_id = server_id
        self.is_revenge = is_revenge

    @classmethod
    def from_json_packet(cls, packet: dict):
        killer = PlayerDTO.from_json_packet(packet["killer"])
        victim = PlayerDTO.from_json_packet(packet["victim"])
        return KillDTO(killer, victim, packet["weapon"], packet["vehicle"], packet["continent"], packet["server_id"],
                       packet["is_revenge"])

    def __repr__(self):
        if self.killer.name == self.victim.name:
            return f"{self.killer} has killed themselves on {self.continent} using {self.weapon}"
        else:
            return f"{self.killer} has killed {self.victim} on {self.continent} using {self.weapon}"
