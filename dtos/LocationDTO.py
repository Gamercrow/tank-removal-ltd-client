from datetime import datetime, timezone


class LocationDTO:

    def __init__(self, player_name: str, facility: str, continent: str, timestamp: int, server_id: int):
        self.player_name = player_name
        self.facility = facility
        self.continent = continent
        self.timestamp = timestamp
        self.server_id = server_id

    @classmethod
    def from_json_packet(cls, packet: dict):
        return LocationDTO(packet["player_name"], packet["facility"], packet["continent"],
                           packet["timestamp"], packet["server_id"])

    def __repr__(self):
        difference = self.get_time_diff()
        time_difference = divmod(difference.seconds, 60)
        time_difference_text = f"{time_difference[0]} min {time_difference[1]} sec ago"
        time = time_difference_text if difference.seconds > 15 else "just now"

        return f"{self.player_name} was last seen at {self.facility} ({self.continent}) {time}"

    # Timestamp for Location data is sent by the Census API as UTC timestamp, so we need to make sure we use the same
    # timezone to compute the difference, regardless of the client computer's timezone settings
    def get_time_diff(self):
        timestamp_now = datetime.now(timezone.utc)
        timestamp_then = datetime.fromtimestamp(self.timestamp, timezone.utc)
        return timestamp_now - timestamp_then
