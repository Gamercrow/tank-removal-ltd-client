COLOR_TR = "\033[91;1m"
COLOR_NC = "\033[94;1m"
COLOR_VS = "\033[95;1m"
COLOR_NSO = "\033[1m"
RESET_FORMATTING = "\033[0m"


class PlayerDTO:

    def __init__(self, name: str, level: int, prestige: int, faction_id: int, outfit_tag: str, kdr: float):
        self.name = name
        self.level = level
        self.prestige = prestige
        self.faction_id = faction_id
        self.outfit_tag = outfit_tag
        self.kdr = kdr

    @classmethod
    def from_json_packet(cls, packet: dict):
        return cls(packet["name"], packet["level"], packet["prestige"], packet["faction_id"],
                   packet["outfit_tag"], packet["kdr"])

    def __repr__(self):
        if self.name == "":
            return "someone"

        match self.faction_id:
            case 1:
                formatting = COLOR_VS
            case 2:
                formatting = COLOR_NC
            case 3:
                formatting = COLOR_TR
            case 4:
                formatting = COLOR_NSO
            case _:
                formatting = RESET_FORMATTING

        if int(self.prestige) == 0:
            general_level = self.level
        else:
            general_level = f"{self.prestige}~{self.level}"

        if self.outfit_tag == "":
            outfit_tag = ""
        else:
            outfit_tag = f"[{self.outfit_tag}] "

        return f"{formatting}{outfit_tag}{self.name} ({general_level} - {self.kdr} KDR){RESET_FORMATTING}"
