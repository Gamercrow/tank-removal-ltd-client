from dtos.PlayerDTO import PlayerDTO


class VehicleKillDTO:

    def __init__(self, killer: PlayerDTO, victim: PlayerDTO, weapon: str, vehicle: str, victim_vehicle: str,
                 continent: str, is_owned: bool, server_id: int, is_revenge: bool):
        self.killer = killer
        self.victim = victim
        self.weapon = weapon
        self.continent = continent
        self.vehicle = vehicle
        self.victim_vehicle = victim_vehicle
        self.is_owned = is_owned
        self.server_id = server_id
        self.is_revenge = is_revenge

    @classmethod
    def from_json_packet(cls, packet: dict):
        killer = PlayerDTO.from_json_packet(packet["killer"])
        victim = PlayerDTO.from_json_packet(packet["victim"])
        return VehicleKillDTO(killer, victim, packet["weapon"], packet["vehicle"], packet["victim_vehicle"],
                              packet["continent"], packet["is_owned"], packet["server_id"], packet["is_revenge"])

    def __repr__(self):
        if self.is_owned:
            if self.killer.name == self.victim.name:
                return f"{self.killer} has destroyed their own {self.victim_vehicle} " \
                       f"on {self.continent} using {self.weapon}"
            else:
                return f"{self.killer} has destroyed {self.victim}'s {self.victim_vehicle} " \
                       f"on {self.continent} using {self.weapon}"
        else:
            return f"{self.killer} has destroyed a {self.victim_vehicle} on {self.continent} using {self.weapon}"
