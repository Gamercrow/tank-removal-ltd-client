﻿# Tank Removal Ltd

An automatic, lightweight, Python 3.10+ CLI program that allow the automatic tracking of Main Battle Tanks (MBTs) 
and Colossus tanks on a specific Planetside 2 server.
This app simply consumes data sent by the Tank Removal Ltd server, which itself consumes data from the
[Census Realtime API](https://census.daybreakgames.com/ps2-websocket.html) and the
[Census API](https://census.daybreakgames.com/).

> **IMPORTANT NOTE :** Tank Removal Ltd's server uses only **publicly-available data**; namely kills, vehicle kills, 
> and base capture events, to compute the data it sends to this client. Everything this program does could be done
> focussing a specific player using other services like [Fisu](https://ps2.fisu.pw/player/) or [Voidwell](https://voidwell.com/).

*Tank Removal Ltd is in no way affiliated with nor endorsed by either Daybreak Games Company or Rogue Planet Games.*\
*Contributors to this project cannot be held responsible of any suspension or ban resulting from any misuse.*

Want to chat with developers of all things Census related? Come say hello in the Planetside Community Developers Discord !\
![Discord](https://img.shields.io/discord/1019343142471880775?color=blue&label=Planetside%20Community%20Developers&logo=discord&logoColor=%2302B4FF)



## Getting Started

Simply download this project and double-click on the `start.bat` for Windows or `start.sh` for MacOS/Linux 
(yet to be implemented and tested)

### Download using [Git](https://git-scm.com/)

In any folder, open the Git console, and enter the following command :
```bash
git clone https://gitlab.com/Gamercrow/tank-removal-ltd-client.git
```
This is the recommended way, since it will allow for easy updates afterwards using the `git pull` command.

### Download the project archive (.zip)

After clicking on the blue `Code` button above, click on the `zip` option.
You can also click [here](https://gitlab.com/Gamercrow/tank-removal-ltd-client/-/archive/master/tank-removal-ltd-client-master.zip)
to download it



## Features

### Kill tracking

This program allows you to display each kill (and vehicle destruction) made using tracked tanks, i.e Prowlers,
Vanguards and Colossi. All of this is done in real time (give or take some seconds), and does no discrimination against
anybody; this means that if someone uses one of these tanks, either as a driver or a gunner, their name will show up
whenever they kill or destroy anyone or anything.\
This program also allows to track "revenge" kills, i.e whenever someone either kill a known tracked tank user,
or destroy said tank. When a tracked tank user gets killed, they won't be tracked anymore, meaning no more "revenge" kills will be
displayed when they get killed, and no more location data will be displayed whenever they capture or defend a base (see below). 

### Location tracking

This program also allows the user to track tank users on the map, using base capture/defense events to do so.
Whenever a kill is made, both the killer and the victim will be checked for any recent known base capture or defense.
Any one that gets found will be displayed (for instance, `Gamercrow's victim K0ngLush was last seen at Nason's Defiance 3min 10sec ago`)
right next to the corresponding kill.
Moreover, when someone gets a kill with a tracked vehicle, it will start a timer of 15 minutes during which any base
they capture or defense will be displayed (for instance, `Gamercrow was last seen at Nason's Defiance just now`).

### Config

Obviously, you may not need or want all of these pieces of information spammed before you, hence the config file.
This will allow you to toggle some features on or off, in addition to select the server on which you want to track tanks.

### Keyboard control

Even though this is a CLI program, that doesn't mean care shouldn't be given to user experience ! Almost every single one
of the configurable flags (see above) can also be changed on the fly. If you ever want a refresher of what can be done, 
simply press **H** (as in Help) to see all possible commands.
To quit, you'll have to press Q twice.

### Logs

Everything you will see displayed on screen will also get logged in a conveniently named log file, and an associated error_log
file will also be created alongside to allow you to more easily spot issues (and possibly report 
them [here](https://gitlab.com/Gamercrow/tank-removal-ltd-client/-/issues)).



## Questions & Answers

**Q: I have an idea for a new feature/I want to file a bug report**\
**A:** Use [Gitlab's Issues](https://gitlab.com/Gamercrow/tank-removal-ltd-client/-/issues)
to file a new ticket.

**Q: Is there support for PS4 servers ?**\
**A:** This program supports every existing version of the game (PC, PS4 EU, PS4 US)

**Q: Where can I see the project's advancement ?**\
**A:** You can see it via Gitlab's issues (see link above).
If you want to see all of these issues neatly organized, what is planned and what is currently being worked on,
use [Gitlab's issue board](https://gitlab.com/Gamercrow/tank-removal-ltd-client/-/boards).

**Q: When updating, my config gets reset to default values, why ?**\
**A:** I haven't yet implemented a way to keep your old config file when updating, however, it is already
planned to be implemented (see Issue #9).

**Q: I'm pretty sure an event should have been displayed, but it has not been, why ?**\
**A:** It is possible for the connexion between the server and the Census Realtime API to drop for some seconds 
(or the connexion between you and the server),
as such, some real-time events might get lost.

**Q: Some events have been displayed in the wrong order, why ?**\
**A:** Events dispatched by the Tank Removal Ltd server have no guarantee about their order because different processes
manage different event types.

**Q: Why aren't Chimeras tracked ?**\
**A:** Chimeras tanks are very underpowered (more than 2 and a half year later and they still have no special ability)
so I decided to not include them in the tracking. Unlike Colossi, Vanguards and Prowlers, they do not represent a great danger,
and can seldom (if ever) disrup fights in a meaningful way.

**Q: Why aren't Magriders tracked ?**\
**A:** Unlike Prowlers and Vanguards, the Magrider actually requires skill to even hope to be effective.

**Q: Can I reuse your code ?**\
**A:** This code is under [GPLv3 licence](https://www.gnu.org/licenses/quick-guide-gplv3.html), meaning you can 
reuse it the way you like (even commercially), as long as you reference clearly this repository as the source.

**Q: Why TF did you do all this ?** (Warning : small rant)\
**A:** As of now, Prowlers are OP, Vanguard are very strong, and both require little to no skill (map knowledge aside) 
to be used effectively. I grew tired of seeing that any player could just pull a Prowler, dash to a hill
hundreds of meters away with the speed of a Harasser, to rack up kills and destroy Sunderers and fights alike
with little to no counterplay; or just do hit-and-run tactics with both the highest DPS 
and average speed/acceleration of any MBT (that can be boosted even further using Barrage).\
Developers are very unlikely to nerf them the way they should be, so I decided to tackle the issue myself.
Initially, I was only playing using a [suicide Flash](https://youtu.be/f9CyZ1qupDs) against mainly Prowlers
(and yelling "{Tank} Removal Ltd at your service" after each kill, but I needed something to help me track MBTs and 
Colossi to get rid of them more effectively, so I started to implement Tank Removal Ltd for myself, after which I
decided to make it partially open-source (only the display, not the logic behind all of this, to make sure
no one will try to modify it to allow for Chimera and/or Magrider tracking).\
Obviously, you're free to implement and run your own server yourself if you really want these features, but as long
as the game is balanced the way it is, I won't.
