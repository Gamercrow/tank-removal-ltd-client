import pygame


class AlarmService:

    def __init__(self, alarm_mode: int):
        self.alarm_colossus = 0
        self.alarm_mbt = 0

        self.current_alarm_for = ""

        pygame.mixer.init()
        self.sound_mixer = pygame.mixer

        self.alarm_mode = 0
        self.set_alarm_mode(alarm_mode)
        self.alarm = self.sound_mixer.Sound('./ressources/alarms/alarm.mp3')
        # This should stay as is, don't push this to the max (1) or say goodbye to your eardrums
        self.alarm.set_volume(0.1)

    def fire_alarm(self, vehicle, player_name):
        colossus_check = self.alarm_colossus and vehicle == "Colossus"
        mbt_check = self.alarm_mbt and vehicle in ["Prowler", "Vanguard"]

        if (colossus_check or mbt_check) and not self.sound_mixer.get_busy():
            self.sound_mixer.stop()
            self.alarm.play()
            self.current_alarm_for = player_name

    def mute_alarm_for(self, victim_name):
        if self.current_alarm_for == victim_name:
            self.sound_mixer.stop()
            self.current_alarm_for = ""

    def set_alarm_mode(self, value: int):
        self.alarm_mode = value % 4
        self.alarm_colossus = self.alarm_mode % 2 == 1  # 1 or 3 work
        self.alarm_mbt = self.alarm_mode >= 2  # 2 or 3 work
        # Then mute any currently running alarm
        self.sound_mixer.stop()
        self.current_alarm_for = ""
