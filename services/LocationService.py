class LocationService:

    def __init__(self, location, server_id, alarm_service):
        self.location = location
        self.server_id = server_id

        self.alarm_service = alarm_service

    def dispatch(self, packet):
        server_check = self.server_id == packet.server_id

        return self.location and server_check
