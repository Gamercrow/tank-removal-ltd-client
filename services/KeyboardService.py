import _thread
import os
import signal
import time

import sshkeyboard

from commons.CommonLogic import print_help


class KeyboardService:

    def __init__(self, kill_service, vehicle_destruction_service, location_service, alarm_service):
        self.kill_service = kill_service
        self.vehicle_destruction_service = vehicle_destruction_service
        self.location_service = location_service
        self.alarm_service = alarm_service

        self.about_to_stop = False
        self.already_stopped = False

        self.timer_to_stop = 5

    def timer_reset_stop(self):
        time.sleep(self.timer_to_stop)  # If no confirmation prompt has been given in 5 seconds, cancel the reset
        if not self.already_stopped:
            self.about_to_stop = False
            print("Quitting canceled automatically. Double-tap Q to quit.")

    def keyboard_controls(self, key, process_id):
        alarm_update_string = "Alarm mode changed to {}"
        match key:
            case "q":
                if self.about_to_stop:
                    print("Quitting ...")
                    self.already_stopped = True
                    sshkeyboard.stop_listening()
                    os.kill(process_id, signal.SIGINT)
                else:
                    print("Are you sure you want to quit ? Press Q again to confirm.")
                    print(f"If you do not want to quit, simply wait {self.timer_to_stop} seconds.")
                    self.about_to_stop = True
                    _thread.start_new_thread(self.timer_reset_stop, ())

            case "h":
                print_help()

            case "a":
                self.alarm_service.set_alarm_mode(self.alarm_service.alarm_mode + 1)
                print(alarm_update_string.format(self.alarm_service.alarm_mode))
            case "0":
                self.alarm_service.set_alarm_mode(0)
                print(alarm_update_string.format(self.alarm_service.alarm_mode))
            case "1":
                self.alarm_service.set_alarm_mode(1)
                print(alarm_update_string.format(self.alarm_service.alarm_mode))
            case "2":
                self.alarm_service.set_alarm_mode(2)
                print(alarm_update_string.format(self.alarm_service.alarm_mode))
            case "3":
                self.alarm_service.set_alarm_mode(3)
                print(alarm_update_string.format(self.alarm_service.alarm_mode))

            case "t":
                self.vehicle_destruction_service.vehicle_kills = not self.vehicle_destruction_service.vehicle_kills
                status = "ON" if self.vehicle_destruction_service.vehicle_kills else "OFF"
                print(f"Detection toggled {status} for Vehicle Kills")

            case "p":
                self.kill_service.prowlers = not self.kill_service.prowlers
                self.vehicle_destruction_service.prowlers = self.kill_service.prowlers
                if self.alarm_service.current_alarm_for == 6:
                    self.alarm_service.sound_mixer.stop()
                status = "ON" if self.kill_service.prowlers else "OFF"
                print(f"Detection toggled {status} for Prowlers")
            case "v":
                self.kill_service.vanguards = not self.kill_service.vanguards
                self.vehicle_destruction_service.vanguards = self.kill_service.vanguards
                if self.alarm_service.current_alarm_for == 5:
                    self.alarm_service.sound_mixer.stop()
                status = "ON" if self.kill_service.vanguards else "OFF"
                print(f"Detection toggled {status} for Vanguards")
            case "c":
                self.kill_service.colossi = not self.kill_service.colossi
                self.vehicle_destruction_service.colossi = self.kill_service.colossi
                if self.alarm_service.current_alarm_for == 2007:
                    self.alarm_service.sound_mixer.stop()
                status = "ON" if self.kill_service.colossi else "OFF"
                print(f"Detection toggled {status} for Colossi")

            case "l":
                self.location_service.location = not self.location_service.location
                status = "ON" if self.location_service.location else "OFF"
                print(f"Location display toggled {status}")
            case "r":
                self.kill_service.revenge = not self.kill_service.revenge
                self.vehicle_destruction_service.revenge = self.kill_service.revenge
                status = "ON" if self.kill_service.revenge else "OFF"
                print(f"Revenge display toggled {status}")
            case "s":
                self.alarm_service.sound_mixer.stop()
                print("Alarm stopped")

            case _:
                return
