class KillService:

    def __init__(self, server_id, revenge, prowlers, vanguards, colossi, alarm_service):
        self.server_id = server_id
        self.prowlers = prowlers
        self.vanguards = vanguards
        self.colossi = colossi
        self.revenge = revenge

        self.alarm_service = alarm_service

    def dispatch(self, packet):
        server_check = self.server_id == packet.server_id

        revenge_check = self.revenge and packet.is_revenge
        prowler_check = self.prowlers and packet.vehicle == "Prowler"
        vanguard_check = self.vanguards and packet.vehicle == "Vanguard"
        colossus_check = self.colossi and packet.vehicle == "Colossus"

        should_alarm = server_check and (prowler_check or vanguard_check or colossus_check) and not packet.is_revenge
        should_display = server_check and (revenge_check or prowler_check or vanguard_check or colossus_check)

        if should_alarm:
            self.alarm_service.fire_alarm(packet.vehicle, packet.killer.name)
        else:
            self.alarm_service.mute_alarm_for(packet.victim.name)

        return should_display
