def remove_special_chars(kill_string):
    color_tr = "\033[91;1m"
    color_nc = "\033[94;1m"
    color_vs = "\033[95;1m"
    color_nso = "\033[1m"
    reset_formatting = "\033[0m"

    return kill_string.replace(color_nso, "").replace(color_tr, "").replace(color_nc, "") \
        .replace(color_vs, "").replace(reset_formatting, "")


def print_help():
    print("")
    print("===== Help menu =====")
    print("  H          ---   To display this Help message")
    print("")
    print("  0, 1, 2, 3 ---   To pick the alarm mode : 0 = none, 1 = Colossi only, 2 = Prowlers only, 3 = both")
    print("  A          ---   To cycle through the Alarm modes")
    print("")
    print("  T          ---   To toggle Tanks/vehicles destruction detection")
    print("  C          ---   To toggle Colossi destruction detection")
    print("  P          ---   To toggle Prowlers destruction detection")
    print("  V          ---   To toggle Vanguards destruction detection")
    print("")
    print("  L          ---   To toggle Location info display")
    print("  R          ---   To toggle Revenge kills display")
    print("")
    print("  S          ---   To mute (Stop) the currently playing alarm")
    print("")
    print("  Double-tap Q to close the app")
    print("=====================")
    print("")
