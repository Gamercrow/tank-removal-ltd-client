# Since this is used to check for the version of Python the client has, it should be as backward-compatible as possible
import os
import re


def main():
    versions = os.popen("py --list").read()
    all_versions = re.findall('-V:(\d\.\d+).*', versions)
    valid_versions = re.findall('-V:(3\.1\d).*', versions)
    if len(valid_versions) == 0:
        print("The latest Python version you have is Python " + all_versions[0]
              + " while this program requires Python 3.10 or later")
        print("This program will automatically use the latest Python version found on this computer.")
        exit(0)

    os.system("py -" + valid_versions[0] + " -m pip install -q -r ./ressources/dependencies/requirements.txt")
    os.system("py -" + valid_versions[0] + " main.py")


if __name__ == '__main__':
    main()
