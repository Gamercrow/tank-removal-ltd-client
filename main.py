import os
import re
import signal
import asyncio
import sys
from datetime import datetime
import json
import logging
import threading
import time
import shutil
import tempfile

import urllib.request
from urllib.error import HTTPError, URLError

import sshkeyboard
import websockets
import yaml

from websockets.exceptions import ConnectionClosedError, ConnectionClosedOK

from commons.CommonLogic import print_help, remove_special_chars
from dtos.KillDTO import KillDTO
from dtos.VehicleKillDTO import VehicleKillDTO
from dtos.LocationDTO import LocationDTO
from exceptions.VersionNotLatestError import VersionNotLatestError
from services.AlarmService import AlarmService
from services.KeyboardService import KeyboardService
from services.KillService import KillService
from services.VehicleDestructionService import VehicleDestructionService
from services.LocationService import LocationService


def check_version(config):
    remote_config_url = "https://gitlab.com/Gamercrow/tank-removal-ltd-client/-/raw/master/config/config.yaml"

    try:
        with urllib.request.urlopen(remote_config_url) as response:
            remote_config = yaml.safe_load(response.read().decode('utf-8'))
            if config['version'] != remote_config['version']:
                raise VersionNotLatestError
            print("Tank Removal Ltd is up-to-date.")

    except (HTTPError, KeyError, VersionNotLatestError) as exp:
        match type(exp):
            case HTTPError.__class__:
                print("Can't find the remote config file.")
            case KeyError.__class__:
                print("Can't find the 'version' field in the remote config file.")
            case VersionNotLatestError.__class__:
                pass
        print("This current version is very likely outdated, you should update it.")

        if os.path.isdir(".git"):
            print("Open git bash in the project's root, enter 'git checkout master', then 'git pull'")
        else:
            print("Download the newest version from the following link :")
            print("https://gitlab.com/Gamercrow/tank-removal-ltd-client/-/archive"
                  "/master/tank-removal-ltd-client-master.zip")

    except URLError:
        print("Can't reach the GitLab repo for the config file ...")
        print("This is likely to be an issue with your internet connexion. If not, this might be a bug,"
              "and you may report it via the issue page of this project's repo")


def load_config_with_integrity():
    config_file_name = 'config/config.yaml'
    default_config = {
        "hostname": "localhost",
        "location": True,
        "alarm_mode": 1,
        "prowlers": True,
        "vanguards": True,
        "colossi": True,
        "revenge": True,
        "vehicle_kills": True,
        "version": "1.0.0"
    }

    if not os.path.isfile(config_file_name):
        with open(config_file_name, 'w') as file:
            yaml.dump(default_config, file, sort_keys=False)
            default_config["errors"] = []
            return default_config

    with open(config_file_name, 'r') as file:
        config = yaml.safe_load(file)

        hostname = 'hostname' in config and type(config['hostname']) is str
        location = 'location' in config and type(config['location']) is bool
        alarm_mode = 'alarm_mode' in config and type(config['alarm_mode']) is int and 0 <= config['alarm_mode'] <= 3
        prowlers = 'prowlers' in config and type(config['prowlers']) is bool
        vanguards = 'vanguards' in config and type(config['vanguards']) is bool
        colossi = 'colossi' in config and type(config['colossi']) is bool
        server = 'server' in config and type(config['server']) is int \
                 and config['server'] in [1, 10, 13, 17, 19, 25, 40, 1000, 2000]
        revenge = 'revenge' in config and type(config['revenge']) is bool
        vehicle_kills = 'vehicle_kills' in config and type(config['vehicle_kills']) is bool
        version = 'version' in config and type(config['version']) is str \
                  and re.match('\d+\.\d+\.\d+', config['version']) is not None

        integrity_checks = {
            "hostname": hostname,
            "location": location,
            "alarm_mode": alarm_mode,
            "prowlers": prowlers,
            "vanguards": vanguards,
            "colossi": colossi,
            "server": server,
            "revenge": revenge,
            "vehicle_kills": vehicle_kills,
            "version": version
        }
        integrity_issues = [name for name, integrity in integrity_checks.items() if not integrity]

        # This creates a config using all elements in config, and if there are some missing, the default_config
        # elements will fill the voids (and we also add the integrity issues afterwards)
        return {**default_config, **config, "errors": integrity_issues}


def create_logs_files_names():
    # Note : we use the same number, so that even if a file is automatically deleted because it is empty at the end of
    # the program's execution (often the Error log file), there is no discrepency between the files' numbers afterwards
    current_time = datetime.today().strftime('%Y-%m-%d')

    filename_base = f"logs/{current_time}_"
    error_filename_base = f"error_logs/{current_time}_"
    log_number = 1
    while True:
        current_filename = filename_base + str(log_number) + ".log"
        current_error_filename = error_filename_base + str(log_number) + ".log"
        if not os.path.exists(current_filename) and not os.path.exists(current_error_filename):
            log_file_name = current_filename
            error_log_file_name = current_error_filename
            break
        log_number += 1

    return log_file_name, error_log_file_name


def setup_logger(logger_name, log_file, is_debug, level=logging.INFO):
    root_logger = logging.getLogger(logger_name)
    if is_debug:
        formatter = logging.Formatter('[%(asctime)s] - File: %(filename)s at line %(lineno)d in %(funcName)s \n'
                                      ' [%(levelname)s]: %(message)s', datefmt='%H:%M:%S')
    else:
        formatter = logging.Formatter('[%(asctime)s] [%(levelname)s]: %(message)s', datefmt='%H:%M:%S')
    file_handler = logging.FileHandler(log_file, mode='w', encoding='utf-8')
    file_handler.setFormatter(formatter)

    root_logger.setLevel(level)
    root_logger.addHandler(file_handler)


def clear_empty_logs():
    logger_file = logging.getLogger("logger").handlers[0].baseFilename
    error_logger_file = logging.getLogger("error_logger").handlers[0].baseFilename
    logging.shutdown()

    if os.stat(logger_file).st_size == 0:
        os.remove(logger_file)
    if os.stat(error_logger_file).st_size == 0:
        os.remove(error_logger_file)


def create_keyboard_listener(kb_service, process_id):
    sshkeyboard.listen_keyboard(
        # Takes a function with a single parameter "key", but we also need the process_id to be passed
        on_press=(lambda key: kb_service.keyboard_controls(key, process_id)),
        # If not set, defaults to "Esc", and will kill the listener prematurely if the key is pressed
        until=None
    )


async def main(config):
    args = sys.argv[1:]
    if len(args) > 0:
        print_help()
        sys.exit(1)

    print("To print the Help message, press H")
    print("Starting ...")

    # If there is any error, print each one of them before stopping
    if config['errors']:
        print("The following config fields have errors :")
        print(config['errors'])
        sys.exit(1)

    hostname = config['hostname']
    location = config['location']
    alarm_mode = config['alarm_mode']
    prowlers = config['prowlers']
    vanguards = config['vanguards']
    colossi = config['colossi']
    server = config['server']
    revenge = config['revenge']
    v_kills = config['vehicle_kills']

    log_file_names = create_logs_files_names()
    setup_logger("logger", log_file_names[0], False)
    setup_logger("error_logger", log_file_names[1], True, logging.WARNING)
    logger = logging.getLogger("logger")
    error_logger = logging.getLogger("error_logger")

    alarm_service = AlarmService(alarm_mode)
    kill_service = KillService(server, revenge, prowlers, vanguards, colossi, alarm_service)
    vkill_service = VehicleDestructionService(v_kills, server, revenge, prowlers, vanguards, colossi, alarm_service)
    location_service = LocationService(location, server, alarm_service)
    keyboard_service = KeyboardService(kill_service, vkill_service, location_service, alarm_service)

    kb_thread = threading.Thread(target=create_keyboard_listener, args=(keyboard_service, os.getpid(),))
    kb_thread.start()

    protocol = "ws" if hostname == "localhost:4200" else "wss"
    uri = f"{protocol}://{hostname}"
    async for websocket in websockets.connect(uri):
        # Yep, that's horrible, but Windows' lack of UNIX signals has forced my hand,
        # since it prevents the use of loop.add_signal_handler
        # At least this (somewhat) cleanly closes the websocket connexion
        signal.signal(signal.SIGINT, lambda sig_number, stackframe: websocket.close())
        print("Connected !")
        try:
            async for message in websocket:
                current_time = datetime.today().strftime('%H:%M:%S')

                try:
                    packet = json.loads(message)
                except json.JSONDecodeError:
                    error_logger.warning(f"Message is not JSON, skipping ...\n"
                                         f"{message}")
                    continue

                try:
                    packet_type = packet["_type"]
                except KeyError:
                    print("Error", message)
                    continue

                try:
                    match packet_type:
                        case "kill":
                            event = KillDTO.from_json_packet(packet)
                            display = kill_service.dispatch(event)
                        case "vehicle_kill":
                            event = VehicleKillDTO.from_json_packet(packet)
                            display = vkill_service.dispatch(event)
                        case "location":
                            event = LocationDTO.from_json_packet(packet)
                            display = location_service.dispatch(event)
                        case _:
                            continue
                except KeyError:
                    error_logger.warning(f"Corrupted packet recieved, skipping ...\n"
                                         f"{packet}")
                    continue

                if display:
                    print(f"[{current_time}]", event)
                    logger.info(f"[{current_time}] {packet} \n"
                                f"-->{remove_special_chars(repr(event))}")
        except (ConnectionRefusedError, ConnectionClosedError, ConnectionClosedOK, TimeoutError):
            print("Server down, retrying in 5 seconds ...")
            time.sleep(5)
        except Exception as exp:
            shutdown_message = "Unmanaged fatal exception, shutting down ..."
            print(shutdown_message)
            print("More infos in the latest error_log")
            error_logger.critical(f"{shutdown_message} \n"
                                  f"{exp}")
            return


if __name__ == '__main__':
    config_data = load_config_with_integrity()
    check_version(config_data)
    asyncio.run(main(config_data))
    clear_empty_logs()
